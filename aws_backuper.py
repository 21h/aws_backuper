'''

 AWSBackuper

 Backup your instance volumes into snapshots and maintain history in days.

 Written by Vladimir Smagin, 2018
   http://blindage.org
   21h@blindage.org

'''
import boto3
import datetime
import config

def checkIfOlder(snapshotTime, histotyInterval):
    delta = datetime.datetime.now().replace(tzinfo=datetime.timezone.utc) - snapshotTime
    if delta.days > histotyInterval:
        return True
    else:
        return False


for what2backup in config.backupImages:
    ec2client = boto3.client(
        service_name='ec2',
        region_name=what2backup['regionID'],
        aws_access_key_id=config.accessKeyID,
        aws_secret_access_key=config.accessKeySecret,
    )
    ec2resource = boto3.resource(
        service_name='ec2',
        region_name=what2backup['regionID'],
        aws_access_key_id=config.accessKeyID,
        aws_secret_access_key=config.accessKeySecret,
    )
    print("Backing up instance",
          what2backup['instanceID'], "in", what2backup['regionID'])
    print("\tCreating snapshot of", what2backup['volumeID'])
    volume = ec2resource.Volume(what2backup['volumeID'])
    snapshot = volume.create_snapshot(
        Description="AWSBackuper: %s" % datetime.datetime.now(),
        TagSpecifications=[
            {   "ResourceType": "snapshot",
                "Tags": [{"Key": "CreatedBy", "Value": "AWSBackuper"}]
            }
        ]
    )    
    snapshots = ec2client.describe_snapshots(
        Filters=[
            {"Name": "volume-id", "Values": [what2backup['volumeID']]},
            {"Name":"tag:CreatedBy","Values": ["AWSBackuper"]}
        ])
    print("\tFound", len(snapshots['Snapshots']),"backup snapshots")
    print("\tRemoving snapshots older than", what2backup['history'], "days")
    for snapshot in snapshots['Snapshots']:
        decision = "passed"
        if checkIfOlder(snapshot["StartTime"], what2backup['history']):
            decision = "removed"
            ec2resource.Snapshot(snapshot["SnapshotId"]).delete()
        print("\t\t",snapshot["SnapshotId"], "...", decision)
    print("Done")

